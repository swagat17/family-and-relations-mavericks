# mavericks-familytree

Command-line application​ to convert the family tree into some digital format.

# Build 
```shell
mvn clean package install
```

# Prerequisite
- Java 8
- Maven 3
- Git

# To Run The Project
#### - Using IDE
- Open compatible IDE find the file named as FamilytreeApplication.
- Right click on that file and run as Java application.
- Go to the console and start using the app

#### - Using CMD
- Go to the project folder location open command prompt.
- Execute ```mvn clean package install``` command.
- Open target folder and run ```java -jar familytree-0.0.1-SNAPSHOT.jar``` command. Now application is ready to use
