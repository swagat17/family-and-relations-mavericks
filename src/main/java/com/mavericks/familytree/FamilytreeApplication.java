package com.mavericks.familytree;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FamilytreeApplication implements CommandLineRunner {

	@Autowired
	private Starter starter;

	public static void main(String[] args) {
		SpringApplication.run(FamilytreeApplication.class, args);
	}

	// System entry point which will start the process
	@Override
	public void run(String... arg0) throws Exception {
		starter.start();
	}
}
