package com.mavericks.familytree;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.mavericks.familytree.component.Mother;
import com.mavericks.familytree.component.Person;
import com.mavericks.familytree.component.Relation;
import com.mavericks.familytree.component.Spouse;
import com.mavericks.familytree.dto.InputDTO;

@Component
public class Starter implements ApplicationContextAware {

	@Autowired
	private static ApplicationContext context;

	private static Map<String, Class<? extends Relation>> map = new HashMap<>();
	static {
		map.put("person", Person.class);
		map.put("mother", Mother.class);
		map.put("husband", Spouse.class);
		map.put("wife", Spouse.class);
	}

	private Scanner scanner = new Scanner(System.in);

	// System's entry point
	public void start() {
		while (true) {
			System.out.println("");
			System.out.print("Input: ");
			String inupt = scanner.nextLine();
			// Create POJO
			InputDTO input = createPojo(inupt);
			// Execute business logic
			if (input != null) {
				Relation relation = builder(input);
				if (relation != null) {
					System.out.println(relation.run());

				}
			}
		}
	}

	// Manipulate the user input and create POJO for further use.
	public InputDTO createPojo(String inupt) {
		InputDTO inputDTO = null;
		String[] inputArray = inupt.trim().split(" ");
		// Check the input take decision depending upon it.
		if (inputArray.length == 2) {
			try {
				inputDTO = new InputDTO();
				String[] lhsInput = inputArray[0].trim().split("=");
				String[] rhsInput = inputArray[1].trim().split("=");
				// Set values to DTO
				inputDTO.setLhs(lhsInput[0]);
				inputDTO.setLhsValue(lhsInput[1]);
				inputDTO.setRhs(rhsInput[0]);
				inputDTO.setRhsValue(rhsInput[1]);
				// System.out.println(inupt);
			} catch (Exception e) {
				// To handle invalid input
				System.out.println("Invalid input.");
			}
		} else {
			System.out.println("Invalid input.");
		}
		return inputDTO;

	}

	// Initialize the required class
	public Relation builder(InputDTO input) {
		Relation relation = null;
		try {
			relation = (Relation) Class.forName(map.get(input.getLhs().toLowerCase()).getName()).newInstance();
		} catch (Exception e) {
			relation = null;
			System.out.println("Invalid keyword type requested: " + input.getLhs());
		}

		if (relation != null) {
			try {
				AutowireCapableBeanFactory factory = context.getAutowireCapableBeanFactory();
				factory.autowireBean(relation);
				factory.initializeBean(relation, input.getLhs());
				relation.initialize(input);
			} catch (IllegalArgumentException iae) {
				relation = null;
			}
		}
		return relation;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;

	}

}
