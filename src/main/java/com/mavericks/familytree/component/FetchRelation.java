package com.mavericks.familytree.component;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mavericks.familytree.dto.InputDTO;
import com.mavericks.familytree.entity.Family;
import com.mavericks.familytree.repository.FamilyRepository;

@Component
public class FetchRelation {

	@Autowired
	private FamilyRepository familyRepository;

	// Find father
	public String fetchFather(Family family, InputDTO input) {
		String result = isLaw(input, family);
		if (!result.trim().equalsIgnoreCase("false")) {
			return result;
		}
		Family familyResult = familyRepository.findParent(family.getFkId());
		return (familyResult.getMale() == null ? familyResult.getInLaw() : familyResult.getMale());
	}

	// Find mother
	public String fetchMother(Family family, InputDTO input) {
		String result = isLaw(input, family);
		if (!result.trim().equalsIgnoreCase("false")) {
			return result;
		}
		Family familyResult = familyRepository.findParent(family.getFkId());
		return (familyResult.getFemale() == null ? familyResult.getInLaw() : familyResult.getFemale());
	}

	// Find brothers
	public String fetchBrothers(Family family, InputDTO input) {
		String result = isLaw(input, family);
		if (!result.trim().equalsIgnoreCase("false")) {
			return result;
		}
		List<String> results = familyRepository.findBrothers(family.getFkId(), input.getLhsValue());
		return fetchResult(input, results);
	}

	// Find sisters
	public String fetchSisters(Family family, InputDTO input) {
		String result = isLaw(input, family);
		if (!result.trim().equalsIgnoreCase("false")) {
			return result;
		}
		List<String> results = familyRepository.findSisters(family.getFkId(), input.getLhsValue());
		return fetchResult(input, results);
	}

	// Find sons
	public String fetchSons(Family family, InputDTO input) {
		List<String> results = familyRepository.findSons(family.getId());
		return fetchResult(input, results);
	}

	// Find daughters
	public String fetchDaughters(Family family, InputDTO input) {
		List<String> results = familyRepository.findDaughters(family.getId());
		return fetchResult(input, results);
	}

	// Find cousins
	public String fetchCousins(Family family, InputDTO input) {
		Family parentFamily = familyRepository.findOne(family.getFkId());
		List<Family> parentSiblings = familyRepository.findAllSiblings(parentFamily.getFkId(), parentFamily.getId());
		if (parentSiblings.isEmpty()) {
			return input.getLhsValue() + " has no cousins";
		} else {
			String names = "";
			for (Family parentSibling : parentSiblings) {
				List<Family> cousinsFamilys = familyRepository.getAllChilds(parentSibling.getId());
				if (!cousinsFamilys.isEmpty()) {
					for (Family cousinsFamily : cousinsFamilys) {
						if (cousinsFamily.getMale() != null)
							names += cousinsFamily.getMale() + ",";
						else
							names += cousinsFamily.getFemale() + ",";
					}
				}
			}
			if (names == "") {
				return input.getLhsValue() + " has no cousins";
			} else {
				names = names.substring(0, names.length() - 1);
				return names;
			}

		}

	}

	// Find grandmother
	public String fetchGrandmother(Family family, InputDTO input) {
		Family parentFamily = familyRepository.findOne(family.getFkId());
		Family grandmotherFamily = null;
		if (parentFamily.getFkId() != null)
			grandmotherFamily = familyRepository.findOne(parentFamily.getFkId());
		if (grandmotherFamily != null) {
			return (grandmotherFamily.getFemale() != null ? grandmotherFamily.getFemale()
					: grandmotherFamily.getInLaw());
		} else {
			return input.getLhsValue() + "'s Grandmother information is not available ";
		}
	}

	// Find grandfather
	public String fetchGrandfather(Family family, InputDTO input) {
		Family parentFamily = familyRepository.findOne(family.getFkId());
		Family grandfatherFamily = null;
		if (parentFamily.getFkId() != null)
			grandfatherFamily = familyRepository.findOne(parentFamily.getFkId());
		if (grandfatherFamily != null) {
			return (grandfatherFamily.getMale() != null ? grandfatherFamily.getMale() : grandfatherFamily.getInLaw());
		} else {
			return input.getLhsValue() + "'s Grandfather information is not available ";
		}
	}

	// Find grandsons
	public String fetchGrandsons(Family family, InputDTO input) {
		List<Family> sonFamilys = familyRepository.findByFkId(family.getId());
		String names = "";
		if (!sonFamilys.isEmpty()) {
			for (Family sonFamily : sonFamilys) {
				List<Family> grandsonsFamilys = familyRepository.findByFkId(sonFamily.getId());
				if (!grandsonsFamilys.isEmpty()) {
					for (Family grandsonsFamily : grandsonsFamilys) {
						if (grandsonsFamily.getMale() != null)
							names += grandsonsFamily.getMale() + ",";
					}
				}
			}
		}
		if (names == "") {
			return input.getLhsValue() + " has no grandson";
		} else {
			names = names.substring(0, names.length() - 1);
			return names;
		}
	}

	// Find grand daughters
	public String fetchGrandaughters(Family family, InputDTO input) {
		List<Family> daughterFamilys = familyRepository.findByFkId(family.getId());
		String names = "";
		if (!daughterFamilys.isEmpty()) {
			for (Family daughterFamily : daughterFamilys) {
				List<Family> grandaughterFamilys = familyRepository.findByFkId(daughterFamily.getId());
				if (!grandaughterFamilys.isEmpty()) {
					for (Family grandaughterFamily : grandaughterFamilys) {
						if (grandaughterFamily.getFemale() != null)
							names += grandaughterFamily.getFemale() + ",";
					}
				}
			}
		}
		if (names == "") {
			return input.getLhsValue() + " has no grandaughter";
		} else {
			names = names.substring(0, names.length() - 1);
			return names;
		}
	}

	// Find aunts and uncles
	public String fetchAuntsAndUncles(Family family, InputDTO input, String auntsOrUncles) {
		Family parentFamily = familyRepository.findOne(family.getFkId());
		String names = "";
		List<Family> siblingsFamilys = familyRepository.findAllSiblings(parentFamily.getFkId(), parentFamily.getId());
		if (!siblingsFamilys.isEmpty()) {
			for (Family siblingsFamily : siblingsFamilys) {
				if (auntsOrUncles.equalsIgnoreCase("aunts")) {
					String name = (siblingsFamily.getFemale() != null ? siblingsFamily.getFemale()
							: siblingsFamily.getInLaw());
					if (name != null)
						names += name + ",";
				} else {
					String name = (siblingsFamily.getMale() != null ? siblingsFamily.getMale()
							: siblingsFamily.getInLaw());
					if (name != null)
						names += name + ",";
				}
			}
		} else {
			return input.getLhsValue() + " has no " + auntsOrUncles;
		}
		if (names == "") {
			return input.getLhsValue() + " has no " + auntsOrUncles;
		} else {
			names = names.substring(0, names.length() - 1);
			return names;
		}
	}

	// Find wife or husband
	public String fetchWifeOrHusband(Family family, InputDTO input, String WifeOrHusband) {
		String result = null;
		if (WifeOrHusband.equalsIgnoreCase("wife")) {
			result = family.getFemale() != null ? family.getFemale() : family.getInLaw();
		} else {
			result = family.getMale() != null ? family.getMale() : family.getInLaw();
		}
		if (result != null)
			return result;
		else
			return input.getLhsValue() + " has no " + WifeOrHusband;
	}

	// Find is the person is son in law or daughter in law
	private String isLaw(InputDTO input, Family family) {
		if (family.getInLaw() != null) {
			if (family.getInLaw().equalsIgnoreCase(input.getLhsValue())) {
				String result = input.getLhsValue() + " is married to "
						+ (family.getMale() == null ? family.getFemale() : family.getMale()) + ". Hence "
						+ input.getRhsValue() + " information is not found!";
				return result;
			} else {
				return "false";
			}
		} else {
			return "false";
		}
	}

	// Generate the result and return it back
	private String fetchResult(InputDTO input, List<String> results) {
		String names = "";
		for (String result : results) {
			if (result != null)
				names += result + ",";
		}
		if (names == "") {
			return "Error: " + input.getLhsValue() + " has no " + input.getRhsValue();
		} else {
			names = names.substring(0, names.length() - 1);
		}
		return names;
	}
}
