package com.mavericks.familytree.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mavericks.familytree.dto.InputDTO;
import com.mavericks.familytree.entity.Family;
import com.mavericks.familytree.repository.FamilyRepository;

@Component
public class Mother extends Relation {

	@Autowired
	private FamilyRepository familyRepository;

	// Mother validation
	@Override
	public void initialize(InputDTO input) {
		if (!input.getRhs().equalsIgnoreCase("Son") && !input.getRhs().equalsIgnoreCase("Daughter")) {
			System.out.println("Error: Incorect rquest");
			System.out.println("Sample: Mother=Zoe Son=Boris");
			throw new IllegalArgumentException();
		}
		this.input = input;
	}

	// Validate mother and son or daughter
	@Override
	public String run() {
		Family family = null;

		family = familyRepository.findByName(input.getRhsValue());
		if (family != null) {
			return "Family member already exists: " + input.getRhsValue();
		}

		family = familyRepository.findByName(input.getLhsValue());
		if (family == null) {
			return "Mother does not exist: " + input.getLhsValue();
		}

		if (!family.getId().equals(1l)) {
			if (family.getInLaw() == null) {
				return input.getLhsValue() + " is not married";
			}
		}

		boolean flag = false;
		if (family.getFemale() != null) {
			if (family.getFemale().equalsIgnoreCase(input.getLhsValue())) {
				flag = true;
			}
		} else {
			if (family.getInLaw().equalsIgnoreCase(input.getLhsValue())) {
				flag = true;
			}
		}

		if (!flag) {
			return "Invalid mother name";
		}

		Family newFamily = new Family();
		if (input.getRhs().equalsIgnoreCase("Son")) {
			newFamily.setMale(input.getRhsValue());
		} else {
			newFamily.setFemale(input.getRhsValue());
		}

		newFamily.setFkId(family.getId());

		familyRepository.save(newFamily);

		return "Output: Welcome to the family, " + input.getRhsValue() + "!";

	}

}
