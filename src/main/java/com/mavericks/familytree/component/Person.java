package com.mavericks.familytree.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mavericks.familytree.dto.InputDTO;
import com.mavericks.familytree.entity.Family;
import com.mavericks.familytree.repository.FamilyRepository;

@Component
public class Person extends Relation {

	@Autowired
	private FamilyRepository familyRepository;

	@Autowired
	private FetchRelation fetchRelation;

	//Validate person
	@Override
	public void initialize(InputDTO input) throws IllegalArgumentException {
		if (!input.getRhs().equalsIgnoreCase("Relation")) {
			System.out.println("Error: Incorect rquest, missing 'Relation'");
			throw new IllegalArgumentException();
		}
		this.input = input;
	}

	//Validate person and fetch corresponding relation
	@Override
	public String run() {
		Family family = familyRepository.findByName(input.getLhsValue());
		String output = "Output: " + input.getRhsValue() + "=";
		boolean rootNode = false;
		if (family == null) {
			
			return "Error: " + input.getLhsValue() + " does not exist.";
		}
		if (family.getId().equals(1l)) {
			rootNode = true;
		}

		switch (input.getRhsValue().toLowerCase()) {
		// To find the father of a given person
		case "father":
			if (rootNode == false)
				return output + fetchRelation.fetchFather(family, input);
			else
				return "Error: This is root node";

		// To find the mother of a given person
		case "mother":
			if (rootNode == false)
				return output + fetchRelation.fetchMother(family, input);
			else
				return "Error: This is root node";

		// To find the brothers of a given person
		case "brothers":
		case "brother":
			if (rootNode == false)
				return output + fetchRelation.fetchBrothers(family, input);
			else
				return "Error: This is root node";

		case "sisters":
		case "sister":
			if (rootNode == false)
				return output + fetchRelation.fetchSisters(family, input);
			else
				return "Error: This is root node";
			
		case "sons":
		case "son":
				return output + fetchRelation.fetchSons(family, input);
			
		case "daughters":
		case "daughter":
				return output + fetchRelation.fetchDaughters(family, input);

		case "cousins":
		case "cousin":
			if (rootNode == false)
				return output + fetchRelation.fetchCousins(family, input);
			else
				return "Error: This is root node";
			
		case "grandfather":
			if (rootNode == false)
				return output + fetchRelation.fetchGrandfather(family, input);
			else
				return "Error: This is root node";
			
		case "grandmother":
			if (rootNode == false)
				return output + fetchRelation.fetchGrandmother(family, input);
			else
				return "Error: This is root node";
			
		case "grandsons":
		case "grandson":
				return output + fetchRelation.fetchGrandsons(family, input);
				
		case "grandaughters":
		case "grandaughter":
				return output + fetchRelation.fetchGrandaughters(family, input);
			
		case "aunts":
		case "aunt":
			if (rootNode == false)
				return output + fetchRelation.fetchAuntsAndUncles(family, input, "aunts");
			else
				return "Error: This is root node";
				
		case "uncles":
		case "uncle":
			if (rootNode == false)
				return output + fetchRelation.fetchAuntsAndUncles(family, input, "uncles");
			else
				return "Error: This is root node";
				
		case "wife":
			return output + fetchRelation.fetchWifeOrHusband(family, input, "wife");
			
		case "husband":
			return output + fetchRelation.fetchWifeOrHusband(family, input, "husband");
			
		default:
			return "Incorect rquest";
		}
		
	}

}
