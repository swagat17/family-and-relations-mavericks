package com.mavericks.familytree.component;

import com.mavericks.familytree.dto.InputDTO;

//Abstract class which get implement by other classes to implements it's abstract method
public abstract class Relation {

	public InputDTO input;

	public abstract void initialize(InputDTO input);

	public abstract String run();
}
