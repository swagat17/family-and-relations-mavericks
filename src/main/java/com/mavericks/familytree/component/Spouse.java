package com.mavericks.familytree.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mavericks.familytree.dto.InputDTO;
import com.mavericks.familytree.entity.Family;
import com.mavericks.familytree.repository.FamilyRepository;

@Component
public class Spouse extends Relation {

	@Autowired
	private FamilyRepository familyRepository;

	//Validate wife or husband
	@Override
	public void initialize(InputDTO input) {
		if ((input.getRhs().equalsIgnoreCase("Husband") && input.getLhs().equalsIgnoreCase("Wife"))
				|| ((input.getRhs().equalsIgnoreCase("Wife") && input.getLhs().equalsIgnoreCase("Husband")))) {
			this.input = input;
		} else {
			System.out.println("Error: Incorect rquest");
			System.out.println("Sample: Husband=Evan Wife=Diana");
			throw new IllegalArgumentException();
		}

	}

	//Add wife or husband
	@Override
	public String run() {

		Family lFamily = familyRepository.findByName(input.getLhsValue());
		Family rFamily = familyRepository.findByName(input.getRhsValue());

		if (lFamily == null && rFamily == null) {
			return "Both members are not part of the family";
		}

		if (lFamily != null && rFamily != null) {
			return"Both members already exist";
		}

		if (lFamily == null) {
			return input.getLhsValue() + " not present hence can't add " + input.getRhs()
			+ " to " + input.getLhsValue();
		}

		if (input.getRhs().trim().equalsIgnoreCase("Wife")) {
			if (lFamily.getMale() != null) {
				if (lFamily.getInLaw() != null) {
					return "Incorect rquest";
				} else {
					lFamily.setInLaw(input.getRhsValue());
				}
			} else {
				if(lFamily.getFemale() != null) {
					return "Incorect rquest";
				}else {
					lFamily.setFemale(input.getRhsValue());
				}
			}
		}

		if (input.getRhs().trim().equalsIgnoreCase("Husband")) {
			if (lFamily.getFemale() != null) {
				if(lFamily.getInLaw() != null) {
					return "Incorect rquest";
				}else {
					lFamily.setInLaw(input.getRhsValue());
				}
			} else {
				if(lFamily.getMale() != null) {
					return"Incorect rquest";
				}else {
					lFamily.setMale(input.getRhsValue());
				}
			}
		}
		familyRepository.save(lFamily);
		return"Output: Welcome to the family, " + input.getRhsValue() + "!";
	}

}
