package com.mavericks.familytree.dto;

public class InputDTO {

	private String lhs;
	private String lhsValue;
	private String rhs;
	private String rhsValue;

	public String getLhs() {
		return lhs;
	}

	public void setLhs(String lhs) {
		this.lhs = lhs;
	}

	public String getLhsValue() {
		return lhsValue;
	}

	public void setLhsValue(String lhsValue) {
		this.lhsValue = lhsValue;
	}

	public String getRhs() {
		return rhs;
	}

	public void setRhs(String rhs) {
		this.rhs = rhs;
	}

	public String getRhsValue() {
		return rhsValue;
	}

	public void setRhsValue(String rhsValue) {
		this.rhsValue = rhsValue;
	}

}
