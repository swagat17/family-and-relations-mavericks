package com.mavericks.familytree.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "family")
public class Family implements Serializable {
	private static final long serialVersionUID = 691511849163644644L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "family_id")
	private Long id;

	@Column(name = "male")
	private String male;

	@Column(name = "female")
	private String female;

	@Column(name = "in_law")
	private String inLaw;

	@Column(name = "fk_family_id")
	private Long fkId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMale() {
		return male;
	}

	public void setMale(String male) {
		this.male = male;
	}

	public String getFemale() {
		return female;
	}

	public void setFemale(String female) {
		this.female = female;
	}

	public String getInLaw() {
		return inLaw;
	}

	public void setInLaw(String inLaw) {
		this.inLaw = inLaw;
	}

	public Long getFkId() {
		return fkId;
	}

	public void setFkId(Long fkId) {
		this.fkId = fkId;
	}

}
