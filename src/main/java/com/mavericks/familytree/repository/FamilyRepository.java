package com.mavericks.familytree.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mavericks.familytree.entity.Family;

public interface FamilyRepository extends JpaRepository<Family, Long> {

	@Query("SELECT f FROM Family as f WHERE LOWER(f.male)=LOWER(:name) OR LOWER(f.female)=LOWER(:name) OR LOWER(f.inLaw)=LOWER(:name)")
	public Family findByName(@Param("name") String name);

	@Query("SELECT f from Family as f WHERE id=:fkId")
	public Family findParent(@Param("fkId") Long fkId);

	@Query("SELECT f.male from Family as f WHERE fkId=:fkId and LOWER(f.male)!=LOWER(:name)")
	public List<String> findBrothers(@Param("fkId") Long fkId, @Param("name") String name);

	@Query("SELECT f.female from Family as f WHERE fkId=:fkId and LOWER(f.female)!=LOWER(:name)")
	public List<String> findSisters(@Param("fkId") Long fkId, @Param("name") String name);

	@Query("SELECT f.male from Family as f WHERE fkId=:id")
	public List<String> findSons(@Param("id") Long id);

	@Query("SELECT f.female from Family as f WHERE fkId=:id")
	public List<String> findDaughters(@Param("id") Long id);

	@Query("SELECT f from Family as f WHERE fkId=:fkId and id!=:id")
	public List<Family> findAllSiblings(@Param("fkId") Long fkId, @Param("id") Long id);

	@Query("SELECT f from Family as f WHERE fkId=:id")
	public List<Family> getAllChilds(@Param("id") Long id);

	public List<Family> findByFkId(Long FkId);
}
