package com.mavericks.familytree;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mavericks.familytree.component.Relation;
import com.mavericks.familytree.dto.InputDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FamilytreeApplication.class)
public abstract class FamilytreeApplicationTests {

	@Autowired
	Starter starter;

	// Write all commonly use logic here, hence all test class can extend and use
	// those functionalities
	public String relatioOprations(String testInput) {
		String result = "";
		InputDTO input = starter.createPojo(testInput);
		if (input != null) {
			Relation relation = starter.builder(input);
			if (relation != null) {
				result = relation.run();
			}
		}
		return result;
	}

	// Compare expected output and actual output
	public void compareExpectedOutput(String expectedOutput, String result) {
		Assert.assertEquals(expectedOutput.trim().toLowerCase(), result.trim().toLowerCase());
	}
}
