package com.mavericks.familytree.relation;

import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import com.mavericks.familytree.FamilytreeApplicationTests;

@Transactional
public class RelationTest extends FamilytreeApplicationTests {

	// Test add son functionality
	@Test
	public void addSon() {
		compareExpectedOutput("Output: Welcome to the family, john!", relatioOprations("Mother=Diana Son=john"));
	}

	// Test add daughter functionality
	@Test
	public void addDaughter() {
		compareExpectedOutput("Output: Welcome to the family, Nisha!", relatioOprations("Mother=Diana Daughter=Nisha"));
	}

	// Test add wife functionality
	@Test
	public void addWife() {
		relatioOprations("Mother=Diana Son=john");
		compareExpectedOutput("Output: Welcome to the family, Juliaa!", relatioOprations("Husband=john Wife=Juliaa"));
	}

	// Test add husband functionality
	@Test
	public void addHusband() {
		relatioOprations("Mother=Diana Daughter=Nisha");
		compareExpectedOutput("Output: Welcome to the family, Adam!", relatioOprations("Wife=Nisha Husband=Adam"));
	}

	// Test fetch father functionality
	@Test
	public void fetchFather() {
		relatioOprations("Mother=Diana Son=john");
		compareExpectedOutput("Output: father=evan", relatioOprations("Person=john Relation=father"));
	}

	// Test fetch mother functionality
	@Test
	public void fetchMother() {
		relatioOprations("Mother=Diana Son=john");
		compareExpectedOutput("Output: mother=Diana", relatioOprations("Person=john Relation=mother"));
	}

	// Test fetch husband functionality
	@Test
	public void fetchHusband() {
		compareExpectedOutput("Output: Husband=Evan", relatioOprations("Person=Diana Relation=Husband"));
	}

	// Test fetch wife functionality
	@Test
	public void fetchWife() {
		compareExpectedOutput("Output: Wife=Diana", relatioOprations("Person=Evan Relation=Wife"));
	}

	// Test fetch sisters functionality
	@Test
	public void fetchSisters() {
		relatioOprations("Mother=Diana Son=john");
		relatioOprations("Mother=Diana Daughter=Nisha");
		compareExpectedOutput("Output: Sisters=Nisha", relatioOprations("Person=john Relation=Sisters"));
	}

	// Test fetch brothers functionality
	@Test
	public void fetchBrothers() {
		relatioOprations("Mother=Diana Son=john");
		relatioOprations("Mother=Diana Daughter=Nisha");
		compareExpectedOutput("Output: Brothers=John", relatioOprations("Person=Nisha Relation=Brothers"));
	}

	// Test fetch sons functionality
	@Test
	public void fetchSon() {
		relatioOprations("Mother=Diana Son=john");
		compareExpectedOutput("Output: Sons=John", relatioOprations("Person=Diana Relation=Sons"));
	}

	// Test fetch daughter functionality
	@Test
	public void fetchDaughter() {
		relatioOprations("Mother=Diana Daughter=Nisha");
		compareExpectedOutput("Output: Daughters=Nisha	", relatioOprations("Person=Diana Relation=Daughters"));
	}
	
	//Check root node validation
	@Test
	public void rootNodeCheck() {
		compareExpectedOutput("Error: This is root node", relatioOprations("Person=Evan Relation=mother"));
	}
}
